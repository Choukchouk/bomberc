#define NBCL 10 /*nombre case longueur*/
#define NBCH 10 /*nombre case hauteur*/
#define LONGUEURFENETRE 1500
#define HAUTEURFENETRE 1000
#define LONGUEURC LONGUEURFENETRE/NBCL
#define HAUTEURC HAUTEURFENETRE/NBCH
#include "terrain.h"

typedef struct{
	int numero;
	int posx;
	int posy;
	int nombre_bombe;
	int puissance_bombe;
	int posxbombe;
	int posybombe;
	int num_bombe;
	time_t time_bombe;
}joueur;

void init_joueur1(joueur *j, terrain *t);

void init_joueur2(joueur *j, terrain *t);

void deplacer_gauche(joueur *j, terrain *t);

void deplacer_droite(joueur *j, terrain *t);

void deplacer_haut(joueur *j, terrain *t);

void deplacer_bas(joueur *j, terrain *t);

int poserBombe(joueur *j, terrain *t);

void explosion(joueur *j, terrain *t);



