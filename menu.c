#include<MLV/MLV_all.h>
#define NBCL 10 
#define NBCH 10 
#define LONGUEURFENETRE 1500
#define HAUTEURFENETRE 1000
#define LONGUEURC LONGUEURFENETRE/NBCL
#define HAUTEURC HAUTEURFENETRE/NBCH
#include "menu.h"




	void bandes_rouges(){
		MLV_draw_line( LONGUEURFENETRE/6,
				0,
				LONGUEURFENETRE/6,
				HAUTEURFENETRE,
				MLV_COLOR_RED);
	}
	void retrecir_ecran(){
		MLV_draw_filled_rectangle(0,
					  0,
                			  LONGUEURFENETRE,
					  HAUTEURFENETRE,
					  MLV_COLOR_WHITE);
		MLV_draw_filled_rectangle(0,
					  0,
                			  LONGUEURFENETRE/5-1,
					  HAUTEURFENETRE,
					  MLV_COLOR_BLACK);
		MLV_draw_filled_rectangle(LONGUEURFENETRE-LONGUEURFENETRE/5,
					  0,
                			  LONGUEURFENETRE,
					  HAUTEURFENETRE,
					  MLV_COLOR_BLACK);
	}

