#include<stdio.h>
#include<stdlib.h>
#include<MLV/MLV_all.h>
#include <time.h>
#include "joueur.h"
#define JEU 1
#define MENU 0
#define FERMER 2
#define OPTION 3

int main( int argc, char * argv[] ){
	int etat = JEU ;
    int  j;
	MLV_Image *image;
    MLV_Image *image2;
	int stop = 1;
	int waitmouse = 1;
    int x, y, i;
    int heure_actuelle[3];

	//initialiser_image_menu();
	MLV_Keyboard_modifier mod;
   	MLV_Keyboard_button sym;
   	MLV_Button_state state;
	MLV_Mouse_button* mouse_button;
    	MLV_Event evenement;
   	MLV_Event event;

	MLV_create_window( "Bomberman", "Bomberman", LONGUEURFENETRE, HAUTEURFENETRE );
	image = MLV_load_image("menu.png");
    image2 = MLV_load_image("menu2.png");
	terrain terrain1;
	joueur j1;
	joueur j2;

	init_terrain(&terrain1);
	init_map(&terrain1);
	init_joueur1(&j1, &terrain1);
	init_joueur2(&j2, &terrain1);
	while(stop>0){
	while(etat == JEU) {

	do{

        time_t debut = time( NULL );
		
		afficherTerrain(&terrain1);
		
		//
        // Récupère un évènement dans la file des évènements
        // non traités.
        //
        event = MLV_get_event( 
                &sym, &mod, NULL, 
                NULL, NULL,
                NULL, NULL, NULL,
                &state
        );
        //
        // Traite l'évènement s'il s'agit d'un évènement clavier
        //
        if( event == MLV_KEY ){
                //
                // Compte le nombre de fois qu'une touche a été
                // préssée.
                //
                if(state == MLV_PRESSED){                          
                	if ( sym == MLV_KEYBOARD_d ){
                		deplacer_droite(&j1, &terrain1);
                	}
                	if ( sym == MLV_KEYBOARD_q ){
                		deplacer_gauche(&j1, &terrain1);
                	}
                	if ( sym == MLV_KEYBOARD_z ){
                		deplacer_haut(&j1, &terrain1);
                	}
                	if ( sym == MLV_KEYBOARD_s ){
                		deplacer_bas(&j1, &terrain1);
                	}
                	if ( sym == MLV_KEYBOARD_RIGHT ){
                		deplacer_droite(&j2, &terrain1);
                	}
                	if ( sym == MLV_KEYBOARD_LEFT ){
                		deplacer_gauche(&j2, &terrain1);
                	}
                	if ( sym == MLV_KEYBOARD_UP ){
                		deplacer_haut(&j2, &terrain1);
                	}
                	if ( sym == MLV_KEYBOARD_DOWN ){
                		deplacer_bas(&j2, &terrain1);
                	}
                	if ( sym == MLV_KEYBOARD_SPACE ){
                        if(poserBombe(&j1, &terrain1) == 1){
                            explosion(&j1, &terrain1);
                        }
                	}
                	if ( sym == MLV_KEYBOARD_RETURN ){
                                   
                        if(poserBombe(&j2, &terrain1) == 1){
                            explosion(&j2, &terrain1);
                        }
                	}
                }
        }

	}while(
            !(
                    ( sym == MLV_KEYBOARD_k ) &&
                    MLV_shift_key_was_pressed( mod ) &&
                    ( state == MLV_RELEASED )
            ) 
    );

	MLV_free_window();

	return 0;
}
	while (etat == MENU){
        retrecir_ecran();
        MLV_draw_image(image,LONGUEURFENETRE/5,0);
        MLV_actualise_window(); 
        MLV_wait_mouse(&x, &y);
        if (y > 300 && y <380 & x>300 & x <1200){
                MLV_clear_window(MLV_COLOR_BLACK);
                etat = JEU;
                        }
        if (y > 500 && y < 560 && x>300 && x<1200){MLV_free_window();}
        if (x > 1060 && y < 120 && x <1200 ) { etat = OPTION ; }
                        }

    while (etat == OPTION){
        retrecir_ecran();
        MLV_draw_image(image2,LONGUEURFENETRE/5,0);
        MLV_actualise_window(); 
        MLV_wait_mouse(&x, &y);
        if (x > 1060 && y < 120 && x <1200 ) { etat = MENU ; }
}
                }
    
    }
